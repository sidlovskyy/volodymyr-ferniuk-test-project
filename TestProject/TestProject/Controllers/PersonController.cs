﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestProject.Controllers
{
    public class PersonController : Controller
    {

        public PersonController() 
        {
            _personRepository = new Domain.Repository.PersonRepository(new SQLRepository.SQLPersonRepository());
        }

        public ActionResult GetAllPerson()
        {
            var result = _personRepository.GetAllPerson();
            return View(result);
        }

        public ActionResult CreatePerson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SavePerson(Domain.DTO.Person person)
        {
            var result = _personRepository.CreatePerson(person);
            return RedirectToAction("GetAllPerson");
        }

        public ActionResult UpdatePerson()
        {
            return View();
        }

        public ActionResult RemovePerson(Int32 id)
        {
            var result = _personRepository.RemovePerson(id);
            return RedirectToAction("GetAllPerson");
        }
                
        private Domain.Repository.PersonRepository _personRepository;
    }
}
