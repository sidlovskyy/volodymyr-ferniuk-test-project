﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public class PersonRepository
    {
        public PersonRepository(IPersonRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Domain.DTO.Person> GetAllPerson()
        {
            return _repository.GetAllPerson();
        }

        public Boolean CreatePerson(Domain.DTO.Person person) 
        {
            return _repository.CreatePerson(person);
        }

        public Boolean RemovePerson(Int32 id)
        {
            return _repository.RemovePerson(id);
        }

        private IPersonRepository _repository;
    }
}
