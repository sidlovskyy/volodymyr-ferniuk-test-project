﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DTO
{
    public class Address
    {
        public Int32 Id { get;set; }
        public string Street { get;set; }
        public string City  { get;set; }
        public String Country  { get;set; }
        public Int32 Zip { get; set; }
    }
}
