﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IPersonRepository
    {
        IEnumerable<Domain.DTO.Person> GetAllPerson();
        Boolean CreatePerson(Domain.DTO.Person person);
        Boolean RemovePerson(Int32 id);
        Boolean EditPerson(Domain.DTO.Person person);
    }
}
