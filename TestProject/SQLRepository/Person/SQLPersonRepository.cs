﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRepository
{
    public class SQLPersonRepository : Domain.IPersonRepository
    {
        public SQLPersonRepository() 
        {
            _dataContext = new DataContainer();
        }

        public IEnumerable<Domain.DTO.Person> GetAllPerson()
        {
            var resultQuery = _dataContext.People.ToList();
            var result = from p in resultQuery
                         select p.CastToDToModel();
            return result;
        }

        public Boolean CreatePerson(Domain.DTO.Person person)
        {
            var newPerson = new Person()
            {
                Name = person.Name,
                Email = person.Email,
                BirthDate = person.DateOfBirth
            };
            _dataContext.People.Add(newPerson);
            var result = _dataContext.SaveChanges() > 0 ? true : false;
            return result;
        }
        public Boolean RemovePerson(Int32 id) 
        {
            var result = false;
            var person = _dataContext.People.FirstOrDefault(m => m.Id == id);
            if(person != null)
            {
                _dataContext.People.Remove(person);
                result = _dataContext.SaveChanges() > 0 ? true : false;
            }
            return result;
        }
        public Boolean EditPerson(Domain.DTO.Person person) 
        {
            var result = false;
            return result;
        }

        private DataContainer _dataContext;
    }
}
