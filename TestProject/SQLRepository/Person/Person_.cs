﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRepository
{
    public partial class Person
    {
        public Domain.DTO.Person CastToDToModel() 
        {
            var result = new Domain.DTO.Person();
            result.Id = this.Id;
            result.Name = this.Name;
            result.Email = this.Email;
            result.Address = this.Addresses.Select(m=>m.CastToDTOModel()).ToList();
            result.DateOfBirth = this.BirthDate;
            return result;
        }
    }
}
