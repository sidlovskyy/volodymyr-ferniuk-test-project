﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRepository
{
    public partial class Address
    {
        public Domain.DTO.Address CastToDTOModel() 
        {
            var result = new Domain.DTO.Address();
            result.Id = this.Id;
            result.Street = this.Street;
            result.City = this.City;
            result.Country = this.Country;
            result.Zip = this.Zip.HasValue ? this.Zip.Value : 0;
            return result;
        }
    }
}
